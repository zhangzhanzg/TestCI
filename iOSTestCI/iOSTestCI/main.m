//
//  main.m
//  iOSTestCI
//
//  Created by Angel on 16/7/10.
//  Copyright © 2016年 Angel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
